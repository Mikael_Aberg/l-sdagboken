﻿angular.module('starter.services', [])

.factory('Books', function () {

    var books = JSON.parse(window.localStorage.getItem("BookList"));

    if (books == null) {
        books = [];
    }

    var lastSort = 0;

    return {
        
        all: function () {
            return books;
        },

        save: function(){
            window.localStorage.setItem("BookList", JSON.stringify(books));
        },

        size: function(){
            return books.length;
        },

        getIndex: function(book){
            return books.indexOf(book);
        },

        getId: function () {
            var id = 0;
            for (var i = 0; i < books.length; i++) {
                if (books[i].id >= id) {
                    id = books[i].id;
                }
            }
            return (id + 1);
        },

        sort: function(value){
            var tempList = [];
            lastSort = value;

            if (value == 0) {//Title
                
                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });
                if (tempList.length > 0) {
                    tempList.sort(function (a, b) { return a.title.toLowerCase() < b.title.toLowerCase() ? -1 : (a.title.toLowerCase() == b.title.toLowerCase() ? 0 : 1); });

                    var title = tempList[0].title.charAt(0).toUpperCase();

                    for (var i = 0; i < tempList.length; i++) {

                        var currentTitle = tempList[i].title.charAt(0).toUpperCase();

                        if (i === 0) {
                            tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                        } else {

                            if (title < currentTitle) {
                                tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                                title = currentTitle;
                            }
                        }
                    }
                }
            }
            else if (value == 1) {// Rating

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });
                if (tempList.length > 0) {
                    tempList.sort(function (a, b) { return a.rating > b.rating ? -1 : (a.rating == b.rating ? 0 : 1); });

                    var title = tempList[0].rating;

                    for (var i = 0; i < tempList.length; i++) {

                        var currentTitle = tempList[i].rating;

                        if (i === 0) {
                            if (title == -1) {
                                tempList.splice(0, 0, { id: -1, isCat: true, title: 'Unrated' });
                            } else {
                                tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                            }
                        } else {
                            if (title > currentTitle) {
                                if (currentTitle == -1) {
                                    tempList.splice(i, 0, { id: -1, isCat: true, title: 'Unrated' });
                                    title = currentTitle;
                                } else {
                                    tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                                    title = currentTitle;
                                }
                            }
                        }
                    }
                }
            }
            else if (value == 2) {// Author
                var noAuthor = [];

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    if (item.author.length > 0) {
                        tempList.push(item);
                    } else {
                        noAuthor.push(item);
                    }
                });
                if (tempList.length > 0) {
                    tempList.sort(function (a, b) { return a.author.toLowerCase() < b.author.toLowerCase() ? -1 : (a.author.toLowerCase() == b.author.toLowerCase() ? 0 : 1); });

                    var title = tempList[0].author.charAt(0).toUpperCase();

                    for (var i = 0; i < tempList.length; i++) {

                        var currentTitle = tempList[i].author.charAt(0).toUpperCase();

                        if (i === 0) {
                            tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                        } else {

                            if (title < currentTitle) {
                                tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                                title = currentTitle;
                            }
                        }
                    }
                }
                if (noAuthor.length > 0) {
                    tempList.push({ id: -1, isCat: true, title: 'Not specified' });

                    for (var i = 0; i < noAuthor.length; i++) {
                        tempList.push(noAuthor[i]);
                    }
                }
            }
            else if (value == 3) { // Date

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });
                if (tempList.length > 0) {
                    tempList.sort(function (a, b) {
                        a = new Date(a.start);
                        b = new Date(b.start);
                        return a > b ? -1 : (a == b ? 0 : 1);
                    });

                    var title = tempList[0].start;

                    for (var i = 0; i < tempList.length; i++) {

                        var currentTitle = tempList[i].start;

                        if (i === 0) {
                            tempList.splice(0, 0, { id: -1, isCat: true, title: new Date(title).toDateString() });
                        } else {

                            if (title > currentTitle) {
                                tempList.splice(i, 0, { id: -1, isCat: true, title: new Date(currentTitle).toDateString() });
                                title = currentTitle;
                            }
                        }
                    }
                }
            }

            books.length = 0;
            for (var i = 0; i < tempList.length; i++) {
                books.push(tempList[i]);
            }
        },

        remove: function (book) {
            for (var i = 0; i < books.length; i++) {
                if (books[i].id === parseInt(book)) {
                    books.splice(i, 1);
                    break;
                }
            }
        },

        lastSort: function(){
            return lastSort;
        },

        get: function (bookId) {
            for (var i = 0; i < books.length; i++) {
                if (books[i].id === parseInt(bookId)) {
                    return books[i];
                }
            }
        },
    };
});