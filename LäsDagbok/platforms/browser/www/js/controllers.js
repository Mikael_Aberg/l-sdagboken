﻿angular.module('starter.controllers', [])

.controller('listCtrl', function ($scope, Books) {
    $scope.sortValue = 0;

    $scope.switch = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
    };

    $scope.list = Books.all();

    $scope.detele = function (book) {
        Books.remove(book);
    };

    $scope.dateToText = function (date) {
        var textDate = new Date(date);
        return textDate.toDateString();
    };

    angular.element(document).ready(function () {
        changeSort(0);
    });

    changeSort = function (value) {
        Books.sort(value);
        $scope.$apply();
    };
})

.controller('addCtrl', function ($scope, $state, $ionicPopup, Books) {
    $scope.data = {};
    $scope.data.title = '';
    $scope.list = Books.all();
    $scope.genre = [
    { text: 'Adventure', checked: false },
    { text: 'Art', checked: false },
    { text: 'Children\'s', checked: false },
    { text: 'Comic Book', checked: false },
    { text: 'Fantasy', checked: false },
    { text: 'Horror', checked: false },
    { text: 'Literature', checked: false },
    { text: 'Mystery', checked: false },
    { text: 'Romance', checked: false },
    { text: 'Science Fiction', checked: false },
    { text: 'Young Adult', checked: false },
    { text: 'History', checked: false },
    { text: 'How To', checked: false },
    { text: 'Memoir', checked: false },
    { text: 'Science', checked: false }
    ];

    $scope.text = '';
    $scope.data.rating = 5;
    $scope.reading = false;
    $scope.borrowed = false;
    $scope.lent = false;

    angular.element(document).ready(function () {
        $scope.data.startDate = new Date();
    })

    $scope.addBook = function () {
        var genres = $scope.genre.filter(function (a) { return a.checked; });
        var index = 0;

        $scope.list.splice(0, 0, {
            id: Books.getId(),
            author: ($scope.data.author != null && $scope.data.author != '') ? $scope.data.author : '',
            start: $scope.data.startDate.toDateString(),
            end: ($scope.data.showFinish) ? $scope.data.finishDate.toDateString() : '',
            title: $scope.data.title,
            rating: ($scope.reading) ? $scope.data.rating : -1,
            comments: $scope.data.comments,
            genre: genres,
        });

        Books.sort(Books.lastSort());

        $state.go('index');
    }

    $scope.changeButton = function (button) {

        switch (button) {
            case ('f'):
                $scope.reading = true;
                document.getElementById('still').className = 'button button-stable card';
                document.getElementById('finished').className = 'button button-positive card';
                $scope.data.stopDate = new Date();
                break;
            case ('s'):
                $scope.reading = false;
                document.getElementById('finished').className = 'button button-stable card';
                document.getElementById('still').className = 'button button-positive card';
                break;
            case ('b'):
                $scope.borrowed = !$scope.borrowed;

                if ($scope.borrowed) {
                    $scope.data.borrowedUntil = new Date();
                    document.getElementById('borrowed').className = 'button button-positive card';
                } else {
                    document.getElementById('borrowed').className = 'button button-stable card';
                }
                break;
            case ('l'):
                $scope.lent = !$scope.lent;
                if ($scope.lent) {
                    $scope.data.lentUntil = new Date();
                    document.getElementById('lent').className = 'button button-positive card';
                } else {
                    document.getElementById('lent').className = 'button button-stable card';
                }
                break;
        }
    }

    $scope.selectGenre = function () {
        var selectPopUp = $ionicPopup.show({
            title: 'Select Genre',
            template: '<ion-list>'
                + '<ion-checkbox class="checkbox-calm" ng-repeat="item in genre" ng-model="item.checked">{{item.text}}</ion-checkbox>'
                + '</ion-list>',
            scope: $scope,
            buttons: [
                {text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e){
                    $scope.text = "";

                    $scope.genre.filter(function (e) { return e.checked; }).forEach(function (e) { $scope.text += e.text + ', '; });

                    $scope.text = $scope.text.slice(0, $scope.text.length-2);
                }}
            ]
        })
    }
})

.controller('detailsCtrl', function ($scope, $stateParams, Books) {
    $scope.book = Books.get($stateParams.id);

    angular.element(document).ready(function () {
        $scope.testBook = JSON.parse(JSON.stringify($scope.book));
        $scope.testBook.start = new Date($scope.testBook.start);

        if ($scope.testBook.finished) {
            $scope.testBook.end = new Date($scope.testBook.end);
        }

    })

    $scope.edit = false;

    $scope.changeEdit = function () {
        if ($scope.edit) {
            save();
            document.getElementById('editButton').innerHTML = 'Edit';
            $scope.edit = false;
        } else {
            document.getElementById('editButton').innerHTML = 'Save';
            $scope.edit = true;
        }
    }

    function save() {
        
        for (var property in $scope.testBook) {
            if ($scope.testBook.hasOwnProperty(property)) {
                $scope.book[property] = $scope.testBook[property];
            }
        }
        Books.sort(Books.lastSort());
    }
})