﻿angular.module('starter.services', [])

.factory('Books', function () {

    var books = [
                {
                    id: 1,
                    author: 'Natalie Morris',
                    start: '2016-07-03',
                    end: '',
                    title: 'A Legendary History of Javelins',
                    rating: -1,
                    borrowed: true,
                    borrowed_person: 'Elias Robbins',
                    borrowed_until:'2016-07-15', 
                    finished: false,
                }, {
                    id: 2,
                    author: 'Elias Ball',
                    start: '2016-07-03',
                    end: '2010-06-12',
                    title: 'The Sea Goddesses\' Falsehoods',
                    rating: 5,
                    finished: true,
                }, {
                    id: 3,
                    author: 'Wade Hayes',
                    start: '2003-01-13',
                    end: '2010-06-12',
                    title: 'A Wyvern\'s Sexual Habits',
                    rating: 3,
                    finished: true,
                }, {
                    id: 4,
                    author: 'Shelia	Robbins',
                    start: '2007-11-07',
                    end: '',
                    title: 'Prayers to the Money God',
                    rating: -1,
                    finished:false,
                }, {
                    id: 5,
                    author: 'Phyllis Francis',
                    start: '2010-05-10',
                    end: '2010-06-12',
                    finished: true,
                    lent: true,
                    lent_person: 'Phyllis Ball',
                    lent_until: '2016-07-10',
                    title: 'Window of Someone',
                    rating: 5,
                }, {
                    id: 6,
                    author: 'Jan Sparks',
                    start: '2010-01-04',
                    end: '2010-06-12',
                    title: 'Serpent of Person',
                    rating: 7,
                    finished: true,
                }
    ];
    var lastSort = 0;

    return {
        

        all: function () {
            return books;
        },

        size: function(){
            return books.length;
        },

        getIndex: function(book){
            return books.indexOf(book);
        },

        getId: function () {
            var id = 0;
            for (var i = 0; i < books.length; i++) {
                if (books[i].id >= id) {
                    id = books[i].id;
                }
            }
            return (id + 1);
        },

        sort: function(value){
            var tempList = [];
            lastSort = value;

            if (value == 0) {//Title

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });

                tempList.sort(function (a, b) { return a.title.toLowerCase() < b.title.toLowerCase() ? -1 : (a.title.toLowerCase() == b.title.toLowerCase() ? 0 : 1); });

                var title = tempList[0].title.charAt(0);

                for (var i = 0; i < tempList.length; i++) {

                    var currentTitle = tempList[i].title.charAt(0).toUpperCase();

                    if (i === 0) {
                        tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                    } else {

                        if (title < currentTitle) {
                            tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                            title = currentTitle;
                        }
                    }
                }
            }
            else if (value == 1) {// Rating

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });

                tempList.sort(function (a, b) { return a.rating > b.rating ? -1 : (a.rating == b.rating ? 0 : 1); });

                var title = tempList[0].rating;

                for (var i = 0; i < tempList.length; i++) {

                    var currentTitle = tempList[i].rating;

                    if (i === 0) {
                        tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                    } else {
                        if (title > currentTitle) {
                            if (currentTitle == -1) {
                                tempList.splice(i, 0, { id: -1, isCat: true, title: 'Unrated' });
                                title = currentTitle;
                            } else {
                                tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                                title = currentTitle;
                            }
                        }
                    }
                }
            }
            else if (value == 2) {// Author
                var noAuthor = [];

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    if (item.author != '') {
                        tempList.push(item);
                    } else {
                        noAuthor.push(item);
                    }
                });

                tempList.sort(function (a, b) { return a.author.toLowerCase() < b.author.toLowerCase() ? -1 : (a.author.toLowerCase() == b.author.toLowerCase() ? 0 : 1); });

                var title = tempList[0].author.charAt(0);

                for (var i = 0; i < tempList.length; i++) {

                    var currentTitle = tempList[i].author.charAt(0).toUpperCase();

                    if (i === 0) {
                        tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                    } else {

                        if (title < currentTitle) {
                            tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                            title = currentTitle;
                        }
                    }
                }

                if (noAuthor.length > 0) {
                    tempList.push({ id: -1, isCat: true, title: 'Not specified' });

                    for (var i = 0; i < noAuthor.length; i++) {
                        tempList.push(noAuthor[i]);
                    }
                }
            }
            else if (value == 3) { // Date

                books.filter(function (a) { return !a.isCat; }).forEach(function (item) {
                    tempList.push(item);
                });

                tempList.sort(function (a, b) {
                    a = new Date(a.start);
                    b = new Date(b.start);
                    return a > b ? -1 : (a == b ? 0 : 1);
                });

                var title = tempList[0].start;

                for (var i = 0; i < tempList.length; i++) {

                    var currentTitle = tempList[i].start;

                    if (i === 0) {
                        tempList.splice(0, 0, { id: -1, isCat: true, title: title });
                    } else {

                        if (title > currentTitle) {
                            tempList.splice(i, 0, { id: -1, isCat: true, title: currentTitle });
                            title = currentTitle;
                        }
                    }
                }
            }

            books.length = 0;
            for (var i = 0; i < tempList.length; i++) {
                books.push(tempList[i]);
            }
        },

        remove: function (book) {
            for (var i = 0; i < books.length; i++) {
                if (books[i].id === parseInt(book)) {
                    books.splice(i, 1);
                    break;
                }
            }
        },

        lastSort: function(){
            return lastSort;
        },

        get: function (bookId) {
            for (var i = 0; i < books.length; i++) {
                if (books[i].id === parseInt(bookId)) {
                    return books[i];
                }
            }
        },
    };
});